clear all
close all
%%
workdir = 'D:\git-projects\to-bee-or-not-to-bee\ColonyC\';
scandir = '35mm-2gC\';
% atms = ['1st-atm\'; '2nd-atm\'; '3rd-atm\'; '5th-atm\'; '6th-atm\'; '7th-atm\'; '8th-atm\'; '9th-atm\'];
atms = ['10th-atm\'; '11th-atm\'];
movements = ['1st-mov-new'; '2nd-mov-new'; '3rd-mov-new'; '4th-mov-new'; '5th-mov-new'; '6th-mov-new'; '7th-mov-new'; '8th-mov-new'; '9th-mov-new'];
%%
for index1 = 1 : 8
    
    for index2 = 1 : 9
        
      workspacedir = [workdir scandir atms(index1,:) movements(index2,:)]
      if exist(workspacedir)
        
        cd(workspacedir)

        filename = [movements(index2,:) '.mat']
        load(filename)

        n_bees = n_oct/beearea;
        total_bees = sum(n_bees,1);

        corrected_force = zeros(n_frames,2);
        corrected_force_mag = zeros(n_frames,1);

        frames = 1:1:n_frames;
        for i = 1 : n_frames
            corrected_force(i,:) = sum(repmat(n_bees(:,i),1,2) .* v);
            corrected_force_mag(i) = norm(corrected_force(i,:));
        end

        corrected_force_per_bee = corrected_force_mag ./ transpose(total_bees);
        %plot(frames,corrected_force_per_bee)
        
        p1a = figure();
        plot(frames,corrected_force_mag,'*-')
        title('net force (corrected)')
        xlabel('frame no.')
        ylabel('net force (a.u.)')
        saveas(p1a,'corrected_net_force.fig')
        saveas(p1a,'corrected_net_force.png')
        dlmwrite('corrected_net_force.txt',corrected_force_mag,'\n')
        close all
        
        p2a = figure();
        plot(frames,transpose(norm_force)./sum(d_oct(:,:),1),'*-')
        title('net force per bee (a.u.)')
        xlabel('frame no.')
        % ylabel('net force per bee (a.u.)')
        saveas(p2a,'corrected_net_force_perbee.fig')
        saveas(p2a,'corrected_net_force_perbee.png')
        dlmwrite('corrected_net_force_perbee.txt',corrected_force_per_bee,'\n')
        close all
        
        p3a = figure();
        plot(frames,total_bees,'*-')
        title('total # bees (corrected)')
        xlabel('frame no.')
        % ylabel('net force per bee (a.u.)')
        saveas(p3a,'corrected_n_bees.fig')
        saveas(p3a,'corrected_n_bees.png')
        dlmwrite('corrected_n_bees.txt',total_bees,'\n')
        close all

        
        p4a = figure();
        plot(frames,std(n_bees(:,:)),'*-')
        title('std in # bees (spread)')
        xlabel('frame no.')
        saveas(p4a,'corrected_std_bees.fig')
        saveas(p4a,'corrected_std_bees.png')
        dlmwrite('corrected_std_bees.txt',std(n_bees(:,:)),'\n')
        close all
        
        p5a = figure();
        plot(frames,std(n_bees(:,:))./sum(n_bees(:,:),1),'*-')
        title('std in # bees (spread) / total # bees')
        xlabel('frame no.')
        saveas(p5a,'corrected_std_pertotal.fig')
        saveas(p5a,'corrected_std_pertotal.png')
        dlmwrite('corrected_std_pertotal.txt',std(n_bees(:,:))./sum(n_bees(:,:),1),'\n')
        close all
        
        clearvars -except index1 index2 workdir scandir atms movements
        
        else
          disp(['folder ' workspacedir ' does not exist'])
      end
    end
end
%%
% p6a = figure();
% plot(frames,max(d_oct(:,:)),'*-')
% title('# bees in largest octant')
% saveas(p6,'largest_octant.fig')
%%
% p7a = figure();
% plot(frames,max(d_oct(:,:))./sum(d_oct(:,:),1),'*-')
% title('# bees in largest octant / total')
% saveas(p7,'largest_octant_pertotal.fig')
%% 
% max2 = size(1,n_frames);
% for i = 1  : n_frames
%     dummy = sort(d_oct(:,i));
%     max2(i) = dummy(7)+dummy(8);
% end
%% 
% p8a = figure();
% plot(frames,max2,'*-')
% title('sum of largest 2 octants')
% saveas(p8,'sum_largest2_octant.fig')
%% 
% p9a = figure();
% plot(frames,max2./sum(d_oct(:,:),1),'*-')
% title('sum of largest 2 octants / total')
% saveas(p9,'sum_largest2_octant_pertotal.fig')
% 
