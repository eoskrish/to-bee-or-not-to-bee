workspacedir = '/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/35mm-10gC/1st-mov-new/';
cd(workspacedir)

clear all
load('1st-mov.mat')

no_of_bees = sum(d_oct,1);
dlmwrite('no_of_bees.txt', transpose(no_of_bees))

dlmwrite('net_force.txt', norm_force)
dlmwrite('net_force_per_bee.txt', norm_force ./ transpose(no_of_bees))

std_dev = std(d_oct,1);
dlmwrite('std_dev.txt', transpose(std_dev));
dlmwrite('std_dev_per_total.txt', transpose(std_dev./no_of_bees));

clear all
