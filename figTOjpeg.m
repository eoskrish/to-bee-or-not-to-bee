clear all

directory = '/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/60mm-12gC-unsuc/4th-mov-new';

cd(directory)

%% exporting MATLAB fig files to jpeg

d=dir('*.fig'); % capture everything in the directory with FIG extension

allNames={d.name}; % extract names of all .FIG-file

close all; % close any open figures

for dir_ind=1:length(allNames)
      open(allNames{dir_ind}); % open the FIG-file
      base=strtok(allNames{dir_ind},'.'); % chop off the extension (".fig")
      print('-djpeg',base); % export to JPEG as usual
      close(gcf); % close it ("gcf" returns the handle to the current figure)
end

%% combining plots

close all
clear all

size = '60mm';
wt = '19g';
colony = 'C';

movement = '1st';

% specify the folders to extract the data from
%(movement '-Movement'
dir1a = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-1/' ];
dir1b = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-2/' ];
dir1c = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-3/' ];
dir1d = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-4/' ];
dir1e = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-5/' ];
dir1f = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '-unsuc' '/' '19g-6/' ];
dir2a = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '/' '19g-1/' ];
dir2b = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '/' '19g-2/' ];
dir2c = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '/' '19g-3/'];
dir2d = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '/' '19g-4/'];
dir2e = ['/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/' size '-' wt colony '/' '19g-5/'];

dir = {dir1a dir1b dir1c dir1d dir1e dir1f dir2a dir2b dir2c dir2d dir2e};
fmt = {'-r','or','*r','vr','<r','^r','-g','og','*g','vg','sg'}
f1 = figure()
% move into the directory
nf = 100000;
for dir_ind = 1 : length(dir)
    curr_dir = dir{dir_ind};
    cd(curr_dir)

% cd(dir1)
load([movement '-mov.mat'])

nf = min(n_frames, nf);
end

for dir_ind = 1 : length(dir)
    curr_dir = dir{dir_ind};
    cd(curr_dir)

% cd(dir1)
load([movement '-mov.mat'])

% nf = min(n_frames, nf);

time1 = [1:1:nf];
n_bees1 = sum(d_oct,1);
std_n_bees1 = std(d_oct,1);

figure(f1)
% plot(time1(2:end),n_bees1(2:end)./n_bees1(1:end-1)-1,fmt{dir_ind})
% plot(time1,n_bees1/n_bees1(1),fmt{dir_ind})
plot(time1,n_bees1(1:nf)-n_bees1(1),fmt{dir_ind})
% plot(time1,n_bees1,fmt{dir_ind})
% plot(time1,std_n_bees1,fmt{dir_ind})
% plot(time1,norm_force,fmt{dir_ind})
% plot(time1,norm_force./transpose(n_bees1),fmt{dir_ind})
hold on

end

% % cd(dir2)
% load([movement '-mov.mat'])
% 
% n_bees2 = sum(d_oct,1);
% time2 = [1:1:length(n_bees2)];
% 
% figure(f1)
% plot(time2,n_bees2,'*-r')
hold off
legend('1','2','3','4','5','6','7','8','9','10','11')
xlabel('no of frames (time)','FontSize',20)
% ylabel('net force per bee','FontSize',20)
% ylabel('net force','FontSize',20)
% ylabel('std dev in octants','FontSize',20)
% ylabel('n(t)','FontSize',20)
% ylabel('n(t)/n(0)','FontSize',20)
ylabel('n(t) - n(0)','FontSize',20)
% ylabel('$\frac{\Delta n}{n}$','Interpreter','latex','FontSize',30)

plt_title = [size '-' wt colony];
title(plt_title,'FontSize',20)
% title('$\frac{\Delta n}{n}$','Interpreter','latex','FontSize',20)

% cd('/home/krishiyer/Videos/comparison-figures/')
% img_name = [plt_title '-us.jpg'];
% print('-djpeg',img_name)

%% combining plots with actual numbers of bees

close all
clear all

size = '35mm';
wt = '10g';
colony = 'C';

movement = '1st';

% specify the folders to extract the data from

dir1 = ['/home/krishiyer/Videos/' size '-' wt colony '-unsuc' '/' movement '-Movement'];
dir2 = ['/home/krishiyer/Videos/' size '-' wt colony '/' movement '-Movement'];

% move into the directory

cd(dir1)
load([movement '-mov.mat'])

time1 = [1:1:n_frames];
d_bees1 = sum(d_oct,1);

n_bees1 = (d_bees1 + 0.004)/0.058;

f1 = figure()
plot(time1,n_bees1,'*-')
hold on

cd(dir2)
load([movement '-mov.mat'])

d_bees2 = sum(d_oct,1);
time2 = [1:1:length(d_bees2)];
n_bees2 = (d_bees2 + 0.004)/0.058;

figure(f1)
plot(time2,n_bees2,'*-r')
hold off

legend('unsuc','suc')
xlabel('no of frames (time)','FontSize',20)
ylabel('total #bees','FontSize',20)

plt_title = ['total #bees ' size wt colony ];
title(plt_title,'FontSize',20)

cd('/home/krishiyer/Videos/comparison-figures/')
img_name = [plt_title '-us.jpg'];
print('-djpeg',img_name)