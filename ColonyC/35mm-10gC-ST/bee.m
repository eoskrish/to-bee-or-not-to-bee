
% Here's a better code! This opens the image file containing bees and
% superimposes the circle and the octants on it. 

close all

tic

% read the particular image file
im = imread('scene00831.jpg');

fig = imshow(im);
hold on

% get the size of the image in pixels
im_size = size(im);

% the center of the disc (petri dish) is almost coincides with the image
% center. Maybe there is a offset so you'll have to add or subtract a few
% pixels.
offset_x = -12;
offset_y = -30;    % this behaves in reverse direction

c_x = im_size(2)/2 + offset_x;
c_y = im_size(1)/2 + offset_y;

% what should be the radius of the disc? You'll have to experiment a bit
% with this number too before you get the right size. My estimate was about
% 300, so maybe start with that number?

ir = 153;
%or = 500;

% leave the rest to the code :P

left_x = c_x - ir;
left_y = c_y - ir;

rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
axis equal

line([c_x c_x],[c_y c_y+ir])
line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
line([c_x c_x+ir],[c_y c_y])
line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x],[c_y c_y-ir])
line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x-ir],[c_y c_y])
line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])

% rectangle('Position',[left_x left_y 2*or 2*or],'Curvature', [1 1])
% axis equal
% 
% line([c_x c_x],[c_y c_y+or])
% line([c_x c_x+or/sqrt(2)],[c_y c_y+or/sqrt(2)])
% line([c_x c_x+or],[c_y c_y])
% line([c_x c_x+or/sqrt(2)],[c_y c_y-or/sqrt(2)])
% line([c_x c_x],[c_y c_y-or])
% line([c_x c_x-or/sqrt(2)],[c_y c_y-or/sqrt(2)])
% line([c_x c_x-or],[c_y c_y])
% line([c_x c_x-or/sqrt(2)],[c_y c_y+or/sqrt(2)])

hold off

red_thr = 100;

im2 = zeros(size(im,1),size(im,2));
for i = 1 : size(im,1)
    for j = 1 : size(im,2)
        if im(i,j,1) < red_thr
            im2(i,j) = 1;
        else im2(i,j) = 0;
        end
    end
end
fig2 = figure();
imshow(im2)

hold on

rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
axis equal

line([c_x c_x],[c_y c_y+ir])
line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
line([c_x c_x+ir],[c_y c_y])
line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x],[c_y c_y-ir])
line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x-ir],[c_y c_y])
line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])

hold off

%% creating masks for 8 different regions

% adjust outer radius
or = 500; 

mask1 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(0) && (j-c_y)/(i-c_x) < tan(pi/4)
                mask1(j,i) = 1;
            end
        end
    end
end
mask2 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(pi/4) && (j-c_y)/(i-c_x) < tan(pi/2-0.01)
                mask2(j,i) = 1;
            end
        end
    end
end
mask3 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(pi/2+0.01) && (j-c_y)/(i-c_x) < tan(3*pi/4)
                mask3(j,i) = 1;
            end
        end
    end
end
mask4 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(3*pi/4) && (j-c_y)/(i-c_x) < tan(pi)
                mask4(j,i) = 1;
            end
        end
    end
end


mask5 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(0) && (j-c_y)/(i-c_x) < tan(pi/4)
                mask5(j,i) = 1;
            end
        end
    end
end
mask6 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(pi/4) && (j-c_y)/(i-c_x) < tan(pi/2-0.01)
                mask6(j,i) = 1;
            end
        end
    end
end
mask7 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(pi/2+0.01) && (j-c_y)/(i-c_x) < tan(3*pi/4)
                mask7(j,i) = 1;
            end
        end
    end
end
mask8 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(3*pi/4) && (j-c_y)/(i-c_x) < tan(pi)
                mask8(j,i) = 1;
            end
        end
    end
end

% figure()
% imshow(mask1)
            
            
%% coutning the "density" of bees in different octants
% 
% list = importdata('list.txt');
% 
% n_frames = 202;
% start_num = 46;
% end_num = 247;
% 
% for filenum = start_num : end_num
%    
%     imfile = list{filenum};
%     im = imread(imfile);
%     
%     r = im(:,:,1);
%     g = im(:,:,2);
%     b = im(:,:,3);
%     
%     r = r.*uint8(mask1+mask2+mask3+mask4+mask5+mask6+mask7+mask8);
%     g = g.*uint8(mask1+mask2+mask3+mask4+mask5+mask6+mask7+mask8);
%     b = b.*uint8(mask1+mask2+mask3+mask4+mask5+mask6+mask7+mask8);
%     
%     IM = im;
%     IM(:,:,1) = r;
%     IM(:,:,2) = g;
%     IM(:,:,3) = b;
%     
%     imshow(IM)
%     hold on
%     line([c_x c_x],[c_y c_y+or])
%     line([c_x c_x+or/sqrt(2)],[c_y c_y+or/sqrt(2)])
%     line([c_x c_x+or],[c_y c_y])
%     line([c_x c_x+or/sqrt(2)],[c_y c_y-or/sqrt(2)])
%     line([c_x c_x],[c_y c_y-or])
%     line([c_x c_x-or/sqrt(2)],[c_y c_y-or/sqrt(2)])
%     line([c_x c_x-or],[c_y c_y])
%     line([c_x c_x-or/sqrt(2)],[c_y c_y+or/sqrt(2)])
%     hold off
%     
%     outfile = strrep(list(filenum),'.jpg','(count).jpg');
%     saveas(gcf, outfile{1}, 'jpg')
%     
%     close all
%     
% end

%% extending analysis to different time points

close all

list = importdata('list.txt');

start_num = 84;
end_num = 163;
n_frames = end_num - start_num +1;


d_oct = zeros(8,n_frames);

for filenum = start_num : end_num

imfile = list{filenum};
im = imread(imfile);

% fig = imshow(im);
% hold on

im2 = zeros(size(im,1),size(im,2));
for i = 1 : size(im,1)
    for j = 1 : size(im,2)
        if im(i,j,1) < red_thr
            im2(i,j) = 1;
        else im2(i,j) = 0;
        end
    end
end
% fig2 = figure();
% imshow(im2)

% hold on

% rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
% axis equal

% line([c_x c_x],[c_y c_y+ir])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% line([c_x c_x+ir],[c_y c_y])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x],[c_y c_y-ir])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x-ir],[c_y c_y])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% 
% hold off


oct1 = im2.*mask1;
oct2 = im2.*mask2;
oct3 = im2.*mask3;
oct4 = im2.*mask4;
oct5 = im2.*mask5;
oct6 = im2.*mask6;
oct7 = im2.*mask7;
oct8 = im2.*mask8;

d_oct(1,filenum-start_num+1) = size(oct1(oct1>0))/size(mask1(mask1>0));
d_oct(2,filenum-start_num+1) = size(oct2(oct2>0))/size(mask2(mask2>0));
d_oct(3,filenum-start_num+1) = size(oct3(oct3>0))/size(mask3(mask3>0));
d_oct(4,filenum-start_num+1) = size(oct4(oct4>0))/size(mask4(mask4>0));
d_oct(5,filenum-start_num+1) = size(oct5(oct5>0))/size(mask5(mask5>0));
d_oct(6,filenum-start_num+1) = size(oct6(oct6>0))/size(mask6(mask6>0));
d_oct(7,filenum-start_num+1) = size(oct7(oct7>0))/size(mask7(mask7>0));
d_oct(8,filenum-start_num+1) = size(oct8(oct8>0))/size(mask8(mask8>0));

 
% figure()
% bar([1:1:8], d_oct(:,filenum-start_num+1))
% xlabel('octant #')
% ylabel('relative area covered by bees')
% title(imfile)
% outfile = strrep(list(filenum),'jpg','png');
% saveas(gcf, outfile{1}, 'png')


% close all

end
%%

v = zeros(8,2);
force = zeros(n_frames,2);
norm_force = zeros(n_frames,1);

for i = 1 : 8
    v(i,1) = cos(pi/8 + (i-1)*pi/4);
    v(i,2) = sin(pi/8 + (i-1)*pi/4);
end
v = -v;

for i = 1 :n_frames
    force(i,:) = sum(repmat(d_oct(:,i),1,2) .* v);
    norm_force(i) = norm(force(i,:));
end

p1 = figure()
plot([1:1:n_frames],norm_force,'*-')
title('net force')
saveas(p1,'net_force.fig')

p2 = figure()
plot([1:1:n_frames],transpose(norm_force)./sum(d_oct(:,:),1),'*-')
title('net force / bee')
saveas(p2,'net_force_perbee.fig')

p3 = figure()
plot([1:1:n_frames],sum(d_oct(:,:),1),'*-')
title('total # bees')
saveas(p3,'n_bees.fig')

p4 = figure()
plot([1:1:n_frames],std(d_oct(:,:)),'*-')
title('std in # bees (spread)')
saveas(p4,'std_bees.fig')

p5 = figure()
plot([1:1:n_frames],std(d_oct(:,:))./sum(d_oct(:,:),1),'*-')
title('std in # bees (spread) / total')
saveas(p5,'std_pertotal.fig')

p6 = figure()
plot([1:1:n_frames],max(d_oct(:,:)),'*-')
title('# bees in largest octant')
saveas(p6,'largest_octant.fig')

p7 = figure()
plot([1:1:n_frames],max(d_oct(:,:))./sum(d_oct(:,:),1),'*-')
title('# bees in largest octant / total')
saveas(p7,'largest_octant_pertotal.fig')

for i = 1  : n_frames
    dummy = sort(d_oct(:,i));
    max2(i) = dummy(7)+dummy(8);
end

p8 = figure()
plot([1:1:n_frames],max2,'*-')
title('sum of largest 2 octants')
saveas(p8,'sum_largest2_octant.fig')

p9 = figure()
plot([1:1:n_frames],max2./sum(d_oct(:,:),1),'*-')
title('sum of largest 2 octants / total')
saveas(p9,'sum_largest2_octant_pertotal.fig')

