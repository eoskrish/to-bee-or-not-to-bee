%% Section 0: Clear all variables and close all figures

close all
reply = input('This will clear all variables in the workspace. Are you sure you want to proceed? [y/n]: ','s');
if strcmp(reply,'y')
    
    clear all
    
    close all
    
end
    
   

bwflag = 0;
flag2 = 0;


%% Section 1: Enter the directory which you want to analyze

dir = '/home/krishiyer/Videos/to-bee-or-not-to-bee/ColonyC/60mm-11gC/9th-atm'

homedir = '/home/krishiyer/Videos/to-bee-or-not-to-bee';

cd(homedir)


cd(dir)

%% Section 2: Enter the name of the test/starting image file, i.e. the image from where you begin your analysis

testfile = 'scene06181.jpg';

close all
im = imread(testfile);
fig = imshow(im);
hold on
im_size = size(im);

disp('Use the "Data Cursor" tool in the figure to estimate the coordinates the of the center of the disk')

%% Section 3: Use the 'obtain coordinates' of three points on the boundary

x1 = 803;
y1 = 569;

x2 = 1349;
y2 = 490;

x3 = 1173;
y3 = 240;

syms g f c
S = solve(x1^2 + y1^2 + 2*g*x1 + 2*f*y1 + c == 0, x2^2 + y2^2 + 2*g*x2 + 2*f*y2 + c == 0, x3^2 + y3^2 + 2*g*x3 + 2*f*y3 + c == 0);

c_x = double(-S.g);
c_y = double(-S.f);
ir = double(sqrt(S.g^2+S.f^2-S.c));

% c_x = input('estimated x-coordinate of the center: ');
% c_y = input('estimated y-coordinate of the center: ');

% d = imdistline;

%% Section 4: Check the circle

% innerdia = input('estimated diameter of the disk: ');

% ir = innerdia/2;

left_x = c_x - ir;
left_y = c_y - ir;

% delete(d)

figure(1)
rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
axis equal

line([c_x c_x],[c_y c_y+ir])
line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
line([c_x c_x+ir],[c_y c_y])
line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x],[c_y c_y-ir])
line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x-ir],[c_y c_y])
line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])

hold off

%% Binarizing the image

red_thr = 90; % <-- red threshold to adjust
blue_thr = 256;

im2 = zeros(size(im,1),size(im,2));
for i = 1 : size(im,1)
    for j = 1 : size(im,2)
        if im(i,j,1) < red_thr && im(i,j,3) < blue_thr
            im2(i,j) = 1;
        else im2(i,j) = 0;
        end
    end
end

if bwflag == 1
    
    figure(fig2)
    
    close
end
fig2 = figure();
imshow(im2)
bwflag = 1;

hold on

rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
axis equal

line([c_x c_x],[c_y c_y+ir])
line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
line([c_x c_x+ir],[c_y c_y])
line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x],[c_y c_y-ir])
line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x-ir],[c_y c_y])
line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])

hold off
disp('Compare the original image to this binarized one. Adjust the red-threshold if you need to')

%% Measure the length of the bee

figure(1)
d = imdistline;

%% enter the bee length measured and create masks for 8 different regions

beelength = input('estimated length of the bee: ');
beewidth = input('estimated width of the bee: ');

or = ir + beelength; 
beearea = pi*beelength*beewidth/4;

delete(d)

mask1 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(0) && (j-c_y)/(i-c_x) < tan(pi/4)
                mask1(j,i) = 1;
            end
        end
    end
end

mask2 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(pi/4) && (j-c_y)/(i-c_x) < tan(pi/2-0.01)
                mask2(j,i) = 1;
            end
        end
    end
end

mask3 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(pi/2+0.01) && (j-c_y)/(i-c_x) < tan(3*pi/4)
                mask3(j,i) = 1;
            end
        end
    end
end

mask4 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y < 0 && (j-c_y)/(i-c_x) > tan(3*pi/4) && (j-c_y)/(i-c_x) < tan(pi)
                mask4(j,i) = 1;
            end
        end
    end
end

mask5 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(0) && (j-c_y)/(i-c_x) < tan(pi/4)
                mask5(j,i) = 1;
            end
        end
    end
end

mask6 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(pi/4) && (j-c_y)/(i-c_x) < tan(pi/2-0.01)
                mask6(j,i) = 1;
            end
        end
    end
end

mask7 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(pi/2+0.01) && (j-c_y)/(i-c_x) < tan(3*pi/4)
                mask7(j,i) = 1;
            end
        end
    end
end

mask8 = zeros(size(im2));
for j = 1 : size(im,1)
    for i = 1 : size(im,2)
        if (i-c_x)^2 + (j-c_y)^2 > ir^2 && (i-c_x)^2 + (j-c_y)^2 < or^2
            if j-c_y > 0 && (j-c_y)/(i-c_x) > tan(3*pi/4) && (j-c_y)/(i-c_x) < tan(pi)
                mask8(j,i) = 1;
            end
        end
    end
end

disp('Masks have been created!')

%% Show the masks and the bees in them

mask = mask1 + mask2 + mask3 + mask4 + mask5 + mask6 + mask7 + mask8;

fig3 = figure();
imshow(im2.*mask)

%% Find the end frame for analysis 

testfile2 = 'scene07791.jpg';
im_end = imread(testfile2);

if flag2 == 1
    figure(3)
    close
end
% fig4 = figure();
figure, imshow(im_end);
flag2 = 1;
hold on
rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
axis equal
line([c_x c_x],[c_y c_y+ir])
line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
line([c_x c_x+ir],[c_y c_y])
line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x],[c_y c_y-ir])
line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
line([c_x c_x-ir],[c_y c_y])
line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
hold off

disp('if the circle coincides with the disk, it hasnt move. Else, it has check a previous frame.')

%% Extending analysis to different time points

close all

list = importdata('list.txt');

start_num = input('enter the start frame number: ');
end_num = input('enter the end frame number: ');
n_frames = end_num - start_num +1;

n_oct = zeros(8,n_frames);
d_oct = zeros(8,n_frames);

for filenum = start_num : end_num

    imfile = list{filenum};
    im = imread(imfile);

    im2 = zeros(size(im,1),size(im,2));
    for i = 1 : size(im,1)
        for j = 1 : size(im,2)
            if im(i,j,1) < red_thr
                im2(i,j) = 1;
            else im2(i,j) = 0;
            end
        end
    end

    oct1 = im2.*mask1;
    oct2 = im2.*mask2;
    oct3 = im2.*mask3;
    oct4 = im2.*mask4;
    oct5 = im2.*mask5;
    oct6 = im2.*mask6;
    oct7 = im2.*mask7;
    oct8 = im2.*mask8;

    n_oct(1,filenum-start_num+1) = size(oct1(oct1>0),1);
    n_oct(2,filenum-start_num+1) = size(oct2(oct2>0),1);
    n_oct(3,filenum-start_num+1) = size(oct3(oct3>0),1);
    n_oct(4,filenum-start_num+1) = size(oct4(oct4>0),1);
    n_oct(5,filenum-start_num+1) = size(oct5(oct5>0),1);
    n_oct(6,filenum-start_num+1) = size(oct6(oct6>0),1);
    n_oct(7,filenum-start_num+1) = size(oct7(oct7>0),1);
    n_oct(8,filenum-start_num+1) = size(oct8(oct8>0),1);

    d_oct(1,filenum-start_num+1) = size(oct1(oct1>0))/size(mask1(mask1>0));
    d_oct(2,filenum-start_num+1) = size(oct2(oct2>0))/size(mask2(mask2>0));
    d_oct(3,filenum-start_num+1) = size(oct3(oct3>0))/size(mask3(mask3>0));
    d_oct(4,filenum-start_num+1) = size(oct4(oct4>0))/size(mask4(mask4>0));
    d_oct(5,filenum-start_num+1) = size(oct5(oct5>0))/size(mask5(mask5>0));
    d_oct(6,filenum-start_num+1) = size(oct6(oct6>0))/size(mask6(mask6>0));
    d_oct(7,filenum-start_num+1) = size(oct7(oct7>0))/size(mask7(mask7>0));
    d_oct(8,filenum-start_num+1) = size(oct8(oct8>0))/size(mask8(mask8>0));

end

v = zeros(8,2);
force = zeros(n_frames,2);
norm_force = zeros(n_frames,1);

for i = 1 : 8
    v(i,1) = cos(pi/8 + (i-1)*pi/4);
    v(i,2) = sin(pi/8 + (i-1)*pi/4);
end
v = -v;

for i = 1 : n_frames
    force(i,:) = sum(repmat(d_oct(:,i),1,2) .* v);
    norm_force(i) = norm(force(i,:));
end

disp('Calculations Done!')

close all

%% Plots

frames = 1:1:n_frames;

p1 = figure();
plot(frames,norm_force,'*-')
title('net force')
saveas(p1,'net_force.fig')

p2 = figure();
plot(frames,transpose(norm_force)./sum(d_oct(:,:),1),'*-')
title('net force / bee')
saveas(p2,'net_force_perbee.fig')

p3 = figure();
plot(frames,sum(n_oct(:,:),1),'*-')
title('total # bees')
saveas(p3,'n_bees.fig')

p4 = figure();
plot(frames,std(n_oct(:,:)),'*-')
title('std in # bees (spread)')
saveas(p4,'std_bees.fig')

p5 = figure();
plot(frames,std(d_oct(:,:))./sum(d_oct(:,:),1),'*-')
title('std in # bees (spread) / total')
saveas(p5,'std_pertotal.fig')

p6 = figure();
plot(frames,max(d_oct(:,:)),'*-')
title('# bees in largest octant')
saveas(p6,'largest_octant.fig')

p7 = figure();
plot(frames,max(d_oct(:,:))./sum(d_oct(:,:),1),'*-')
title('# bees in largest octant / total')
saveas(p7,'largest_octant_pertotal.fig')

max2 = size(1,n_frames);
for i = 1  : n_frames
    dummy = sort(d_oct(:,i));
    max2(i) = dummy(7)+dummy(8);
end

p8 = figure();
plot(frames,max2,'*-')
title('sum of largest 2 octants')
saveas(p8,'sum_largest2_octant.fig')

p9 = figure();
plot(frames,max2./sum(d_oct(:,:),1),'*-')
title('sum of largest 2 octants / total')
saveas(p9,'sum_largest2_octant_pertotal.fig')

%% Unused portions of the code
% 
%% Section 5: Correct the center coordiantes
% 
% text = sprintf('The previous x-coordinate was %f. What should it be now?',c_x);
% c_x = input('estimated x-coordinate of the center: ');
% text = sprintf('The previous y-coordinate was %f. What should it be now?',c_y);
% c_y = input('estimated y-coordinate of the center: ');
% 
% left_x = c_x - ir;
% left_y = c_y - ir;
% 
% figure(1)
% imshow(im);
% hold on
% 
% rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
% axis equal
% 
% line([c_x c_x],[c_y c_y+ir])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% line([c_x c_x+ir],[c_y c_y])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x],[c_y c_y-ir])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x-ir],[c_y c_y])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% 
% hold off

%% Section 6: Correct the diameter of the disk
% 
% d = imdistline;
% text = sprintf('the previous inner dia was %f. What should it be now: ',innerdia);
% innerdia  = input(text);
% 
% ir = innerdia/2;
% left_x = c_x - ir;
% left_y = c_y - ir;
% delete(d)
% 
% figure(1)
% imshow(im);
% hold on
% 
% rectangle('Position',[left_x left_y 2*ir 2*ir],'Curvature', [1 1])
% axis equal
% 
% line([c_x c_x],[c_y c_y+ir])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% line([c_x c_x+ir],[c_y c_y])
% line([c_x c_x+ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x],[c_y c_y-ir])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y-ir/sqrt(2)])
% line([c_x c_x-ir],[c_y c_y])
% line([c_x c_x-ir/sqrt(2)],[c_y c_y+ir/sqrt(2)])
% 
% hold off